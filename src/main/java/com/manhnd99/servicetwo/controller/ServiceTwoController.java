package com.manhnd99.servicetwo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/service-two")
public class ServiceTwoController {
    @GetMapping
    public String getServiceTwo() {
        return "Hello Service Two";
    }
}
